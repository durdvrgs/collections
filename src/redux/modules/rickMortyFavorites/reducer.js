import { addRickMortysFavorite, removeRickMortysFavorite } from "./actions";

export const rickMortyFavoritesReducer = (state = [], action) => {

    switch (action.type) {

        case addRickMortysFavorite:
            const { rickMortysFavorite } = action
            
            return [...state, rickMortysFavorite]
    
        case removeRickMortysFavorite:
            const { filteredList } = action

            return filteredList

        default:
            
        return state;
    };
};