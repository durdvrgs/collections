import "./Favorites.css";

import { useDispatch } from "react-redux";
import { removeRickMortyFavoritesThunk } from "../../redux/modules/rickMortyFavorites/thunk";

const RickMortyFavorites = () => {

  const favorites = JSON.parse(localStorage.getItem("rickMortyFavorites")) || [];
  const removeFavoriteIcon = <i class="fas fa-trash"></i>;
  const dispatch = useDispatch();

  const removeFavorite = (character) => {
    
    dispatch(removeRickMortyFavoritesThunk(character.id));

    window.location.reload();
  };

  const treatBiggerName = (characterName) => {

    let characterNameSplited = characterName.split(" ");
    let nameTreated = characterName;
    let nameTreatedList = []
    let nameHaveMoreThanTwoWords = characterNameSplited.length > 2 ? true : false;
    let firstLetter = 0

    if (nameHaveMoreThanTwoWords) {

        characterNameSplited.map((word) => {
            nameTreatedList.push(word[firstLetter])
        })
        nameTreatedList[nameTreated.length - 1] = characterNameSplited[characterNameSplited.length - 1];
        nameTreated = nameTreatedList.join(". ");
    };

    return nameTreated
  };

  return (
    <section className="favorites-section">
      <h1>Your Favorites Characters:</h1>
      <div className="primary-content">
        <div className="favorites-content">
          {favorites?.map((char, index) => (
            <div key={index} className="tazo-content">
              <div className="remove-icon-content">
                <button onClick={() => removeFavorite(char)}>{removeFavoriteIcon}</button>
              </div>
              <div className="cards-content">
                <img src={char.image} alt="characters" />
                <div>{treatBiggerName(char.name)}</div>
              </div>
            </div>
          ))}
        </div>
      </div>
    </section>
  );
};

export default RickMortyFavorites;
