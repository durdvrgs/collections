import { Route, Switch } from "react-router-dom";
import { AnimatePresence } from "framer-motion";

import Pokemons from "../../pages/Pokemon";
import PokemonFavorites from "../../pages/PokemonsFavorites ";

import RickandMorty from "../../pages/RickandMorty";
import RickMortyFavorites from "../../pages/RickMortyFavorites"
import Home from "../../pages/Home";

const Pages = () => {
  
  return (
    <AnimatePresence>
      <Switch>
        <Route path="/Pokemons">
          <Pokemons />
        </Route>
        <Route path="/PokemonFavorites">
          <PokemonFavorites />
        </Route>

        <Route path="/RickandMorty">
          <RickandMorty />
        </Route>
        <Route path="/RickandMortyFavorites">
          <RickMortyFavorites />
        </Route>

        <Route path="/">
          <Home />
        </Route>
      </Switch>
    </AnimatePresence>
  );
};

export default Pages;
