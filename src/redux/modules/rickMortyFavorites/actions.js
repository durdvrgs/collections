import { addRickMortyFavoritesType, removeRickMortyFavoritesType } from "./actionsType"

export const addRickMortysFavorite = (rickMortysFavorite) => ({
    type: addRickMortyFavoritesType,
    rickMortysFavorite
});

export const removeRickMortysFavorite = (filteredList) => ({
    type: removeRickMortyFavoritesType,
    filteredList
})