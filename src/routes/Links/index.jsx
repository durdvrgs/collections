import { Link } from "react-router-dom";
import "./Links.css";

const Links = () => {
  const arrKeys = ["0", "1", "2"];

  const handleSetBorder = (e) => {
    let NoSelectedItems = arrKeys.filter((item) => item !== e.id);
    NoSelectedItems.map((item) => {
      let link = document.getElementById(item);
      link.style.borderBottom = "none";
    })

    e.style.borderBottom = "3px solid wheat";
  }

  return (
    <div className="Links-header">
      <Link id="0" to="/" onClick={(e) => handleSetBorder(e.target)}>Home</Link>
      <Link id="1" to="/RickandMorty" onClick={(e) => handleSetBorder(e.target)}>Rick and Morty</Link>
      <Link id="2" to="/Pokemons" onClick={(e) => handleSetBorder(e.target)}>Pokemon</Link>
    </div>
  );
};

export default Links;
