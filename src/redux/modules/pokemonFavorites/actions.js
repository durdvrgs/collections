import { addPokemonFavoritesType, removePokemonFavoritesType } from "./actionsTypes";

export const addPokemonFavorites = (pokemonFavorites) => ({
    type: addPokemonFavoritesType,
    pokemonFavorites
});

export const removePokemonFavorites = (newList) => ({
    type: removePokemonFavoritesType,
    newList
});