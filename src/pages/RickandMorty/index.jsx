import axios from "axios";
import { useState, useEffect } from "react";

import { motion } from "framer-motion";

import Characters from "../../components/RickMortyList";
import Buttons from "../../components/Buttons";
import { Link } from "react-router-dom";

import "./RickandMorty.css";

const RickandMorty = () => {
  const [load, setLoad] = useState(false);
  const [charactersList, setCharactersList] = useState([]);
  const [nextUrl, setNextUrl] = useState(
    "https://rickandmortyapi.com/api/character/?page=1"
  );
  const [previousUrl, setPreviousUrl] = useState("");

  const getCharacterts = async (url) => {
    const response = await axios.get(url);
    setCharactersList([...response.data.results]);

    if (response.data.info.next) {
      setNextUrl(response.data.info.next);
    }
    if (response.data.info.prev) {
      setPreviousUrl(response.data.info.prev);
    }
  };

  const nextPage = () => {
    getCharacterts(nextUrl);
  };
  const previousPage = () => {
    getCharacterts(previousUrl);
  };

  useEffect(() => {
    getCharacterts(nextUrl);
  }, []);

  setTimeout(() => {
    setLoad(true)
  }, 6333)

  const favoriteIcon = <i class="fad fa-star"></i>
  const rickMortyPage = (
  <motion.div
    className="rickMortys-content"
    initial={{ opacity: 0 }}
    animate={{ opacity: 1 }}
    exit={{ opacity: 0 }}
    transition={0.6}
  > 
    <div className="link-favorites-content"> 
      <Link to="/RickandMortyFavorites">Favorites {favoriteIcon}</Link>
    </div>
    <Buttons arrowLeft={previousPage} arrowRight={nextPage} />
    <div>
      <Characters list={charactersList} />
    </div>
  </motion.div> 
  )

  const loadingDiv = <div className="loading"/>

  return (
    load ? rickMortyPage : loadingDiv
  )
};

export default RickandMorty;
