import { addRickMortysFavorite, removeRickMortysFavorite } from "./actions";

export const addRickMortyFavoritesThunk = (character) => {
    
    return (dispatch, _) => {

        let charactersList = JSON.parse(localStorage.getItem("rickMortyFavorites")) || [];
        let indexFound = charactersList.findIndex((chart) => chart.id == character);
        let isNotOnTheList = indexFound < 0 ? true : false;

        if (isNotOnTheList) {

            charactersList.push(character);
            let charactersListJson = JSON.stringify(charactersList);
            localStorage.setItem("rickMortyFavorites", charactersListJson);

            dispatch(addRickMortysFavorite(character))
        }
    }
};

export const removeRickMortyFavoritesThunk = (id) => {

    return (dispatch, _) => {

        let charactersList = JSON.parse(localStorage.getItem("rickMortyFavorites")) || [];
        let filteredList = charactersList.filter((chart) => chart.id !== id);

        let filteredListJson = JSON.stringify(filteredList);

        localStorage.setItem("rickMortyFavorites", filteredListJson);

        dispatch(removeRickMortysFavorite(filteredList))
    }
};