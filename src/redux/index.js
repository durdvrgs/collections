import { combineReducers, createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";

import { pokemonFavoritesReducer } from "./modules/pokemonFavorites/reducer";
import { rickMortyFavoritesReducer } from "./modules/rickMortyFavorites/reducer";


const reducers = combineReducers({
    pokemonFavorites: pokemonFavoritesReducer,
    rickMortysFavorite: rickMortyFavoritesReducer

});

export const store = createStore(reducers, applyMiddleware(thunk))

