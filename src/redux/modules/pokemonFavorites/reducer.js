import { addPokemonFavorites, removePokemonFavorites } from "./actions";


export const pokemonFavoritesReducer = (state = [], action) => {

    switch (action.type) {

        case addPokemonFavorites:
            const { pokemonFavorites } = action
            
            return [...state, pokemonFavorites];
        
        case removePokemonFavorites:
            const { newList } = action

            return newList

        default:
            return state;
    }
};