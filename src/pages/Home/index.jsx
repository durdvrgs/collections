import "./Home.css";
import { motion } from "framer-motion";
import Helmet from "react-helmet";
import { useEffect, useState } from "react";

const Home = () => {

  const [helmet, setHelmet] = useState(null)

  const HelmetTag = <Helmet>
    {/* <meta property="og:image" content="https://picsum.photos/1280/720"/>
    <meta property="og:image:type" content="image/jpg" />
    <meta property="og:image:width" content="1280" />
    <meta property="og:image:height" content="720" />       */}
    <title>Home - Collections</title>
  </Helmet>

  const setMetas = () => {

    return setHelmet(HelmetTag)
  }

  useEffect(() => {
    setMetas()
  },[])
  return (
    <>
    {helmet}
    <motion.div
      className="home-content"
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      exit={{ opacity: 0 }}
      transition={0.6}
    >
      <h1>TAZOS Collection</h1>
    </motion.div>
    </>
  );
};

export default Home;
