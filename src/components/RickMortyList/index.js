import "./Characters-content.css";

import { useDispatch } from "react-redux";
import { addRickMortyFavoritesThunk, removeRickMortyFavoritesThunk } from "../../redux/modules/rickMortyFavorites/thunk";

const Characters = (props) => {

  let dispatch = useDispatch();
  let favoriteCharacters = JSON.parse(localStorage.getItem("rickMortyFavorites")) || []

  const setFavoriteChar = (char, btn) => {

    dispatch(addRickMortyFavoritesThunk(char))

    btn.style.filter = "brightness(1)";
  };

  const removeFavorites = (char, btn) => {

   dispatch(removeRickMortyFavoritesThunk(char.id));

   btn.style.filter = "brightness(1)";
   window.alert("Removido dos Favoritos !")
  };

  const treatBiggerName = (characterName) => {

    let characterNameSplited = characterName.split(" ");
    let nameTreated = characterName;
    let nameTreatedList = []
    let nameHaveMoreThanTwoWords = characterNameSplited.length > 2 ? true : false;
    let firstLetter = 0

    if (nameHaveMoreThanTwoWords) {

        characterNameSplited.map((word) => {
            nameTreatedList.push(word[firstLetter])
        })
        nameTreatedList[nameTreated.length - 1] = characterNameSplited[characterNameSplited.length - 1];
        nameTreated = nameTreatedList.join(". ");
    };

    return nameTreated
  };

  const likeIcon = <i class="fas fa-thumbs-up" id="likeIcon"></i>;
  const alreadyLiked = <i class="fas fa-thumbs-up" id="alreadyLiked"></i>;
  const deslikeIcon = <i class="fas fa-thumbs-down" id="deslikeIcon"></i>;

  return (
    <div className="charactersList-content">
      {props.list.map((character, index) => (
        <div key={index} className="tazo-content">
          <div className="likes-content">
            <div onClick={(e) => setFavoriteChar(character, e.target)}>
            {favoriteCharacters.findIndex((chart) => chart.id === character.id) < 0 ? likeIcon : alreadyLiked}
            </div>
            <div onClick={(e) => removeFavorites(character, e.target)}>
              {deslikeIcon}
            </div>
          </div>
          <div className="characters-card">
            <img src={character.image} alt="characters" />
            <div className="character-name">{treatBiggerName(character.name)}</div>
          </div>
        </div>
      ))}
    </div>
  );
};

export default Characters;
