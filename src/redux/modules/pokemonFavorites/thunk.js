import { addPokemonFavorites, removePokemonFavorites } from "./actions"

export const addPokemonFavoritesThunk = (favorite) => {

    return (dispatch, _) => {

        let list = JSON.parse(localStorage.getItem("pokemonFavorites")) || []

        let indexFound = list.findIndex((pokemon) => pokemon.name === favorite.name)
        let isNotOnTheList = indexFound < 0 ? true : false

        if (isNotOnTheList) {

            list.push(favorite)
            let jsonList = JSON.stringify(list)

            localStorage.setItem("pokemonFavorites", jsonList)

            dispatch(addPokemonFavorites(favorite))
        }
    }
};

export const removePokemonFavoritesThunk = (name) => {

    return (dispatch, getState) => {

        const pokemonFavorites = JSON.parse(localStorage.getItem("pokemonFavorites")) || []
        const list = pokemonFavorites.filter((pokemon) => pokemon.name !== name);

        let jsonList = JSON.stringify(list);
        localStorage.setItem("pokemonFavorites", jsonList);

        dispatch(removePokemonFavorites(list));
    }
};