import axios from "axios";
import { useState, useEffect } from "react";
import { motion } from "framer-motion";
import { Link } from "react-router-dom";

import PokemonsList from "../../components/PokemonsList";
import Buttons from "../../components/Buttons";

import "./Pokemons.css";

const Pokemons = () => {
  const [load, setLoad] = useState(false);
  const [pokemonsList, setPokemons] = useState([]);
  const [nextUrl, setNextPage] = useState(
    "https://pokeapi.co/api/v2/pokemon?offset=0&limit=20"
  );
  const [previousUrl, setPreviousPage] = useState("");

  const getPokemons = async (url) => {
    const response = await axios.get(url);
    setPokemons([...response.data.results]);
    if (response.data.next) {
      setNextPage(response.data.next);
    }
    if (response.data.previous) {
      setPreviousPage(response.data.previous);
    }
  };

  const nextPage = () => {
    getPokemons(nextUrl);
  };
  const previousPage = () => {
    getPokemons(previousUrl);
  };

  useEffect(() => {
    getPokemons(nextUrl);
  }, []);

  setTimeout(() => {
    setLoad(true)
  }, 6333)

  const favoriteIcon = <i class="fad fa-star"></i>
  const pokemonPage = (
    <motion.div
      className="pokemons-content"
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      exit={{ opacity: 0 }}
      transition={0.6}
    >
      <div className="link-favorites-content">
        <Link to="/PokemonFavorites">
          Favorites {favoriteIcon}
        </Link>
      </div>
      <Buttons arrowLeft={previousPage} arrowRight={nextPage} />
      <div className="pokemons-list-content">
        <PokemonsList list={pokemonsList} />
      </div>
    </motion.div>
  )
  const loadingDiv = <div className="loading"/>

  return (
    load ? pokemonPage : loadingDiv
  )
};

export default Pokemons;
