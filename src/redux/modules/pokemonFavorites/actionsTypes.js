export const addPokemonFavoritesType = "@pokemonFavorites/ADD"

export const removePokemonFavoritesType = "@pokemonFavorites/REMOVE"