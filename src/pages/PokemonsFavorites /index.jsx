import "./Pokemons-favorites.css";
import { useDispatch } from "react-redux";
import { removePokemonFavoritesThunk } from "../../redux/modules/pokemonFavorites/thunk";

const PokemonFavorites = () => {
  
  const pokemonFavorites = JSON.parse(localStorage.getItem("pokemonFavorites")) || [];
  const removeFavoriteIcon = <i class="fas fa-trash"></i>
  const dispatch = useDispatch();

  const removeFavorite = (pokemon) => {

    dispatch(removePokemonFavoritesThunk(pokemon.name))

    window.location.reload()
  }

  const brokenUrl = (url) => {
    let urlBroken = url.split("/");
    let id = urlBroken[urlBroken.length - 2];
    let pokImage = `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/${id}.png`;

    return pokImage;
  };

  return (
    <section className="pokemons-section">
      <h1>Your Favorites Pokémons</h1>
      <div className="cardsList-content">
        {pokemonFavorites?.map((pokemons, index) => (
          <div key={index} className="tazos-content">
            <div className="pk-remove-icon-content">
              <button id="removeFavorite-button" onClick={() => removeFavorite(pokemons)}>{removeFavoriteIcon}</button>
            </div>
            <div key={index} className="pokemonsCard">
              <img src={brokenUrl(pokemons.url)} alt="pokemon" />
              <p>{pokemons.name}</p>
            </div>
          </div>
        ))}
      </div>
    </section>
  );
};

export default PokemonFavorites;
