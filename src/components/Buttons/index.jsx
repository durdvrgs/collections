import "./Buttons.css";
import ArrowRightIcon from "@material-ui/icons/ArrowRight";
import ArrowLeftIcon from "@material-ui/icons/ArrowLeft";

const Buttons = (props) => {

  return (
    <div className="btn-set-pages">
      <button onClick={props.arrowLeft}>
        <ArrowLeftIcon fontSize="large" />
      </button>
      <button onClick={props.arrowRight}>
        <ArrowRightIcon fontSize="large" />
      </button>
    </div>
  );
};

export default Buttons;
