import "./pokemons.css";

import { useDispatch } from "react-redux";
import { addPokemonFavoritesThunk, removePokemonFavoritesThunk } from "../../redux/modules/pokemonFavorites/thunk";

const PokemonsList = (props) => {

  const pokemonFavorites = JSON.parse(localStorage.getItem("pokemonFavorites")) || [];
  const likeIcon = <i class="fas fa-thumbs-up" id="likeIcon"></i>;
  const alreadyLiked = <i class="fas fa-thumbs-up" id="alreadyLiked"></i>;
  const deslikeIcon = <i class="fas fa-thumbs-down" id="deslikeIcon"></i>;
  const dispatch = useDispatch();

  const setFavoritePokemon = (pokemon, btn) => {

    dispatch(addPokemonFavoritesThunk(pokemon))

    btn.style.filter = "brightness(1)";
  };

  const removefavoritePokemons = (pokemon, btn) => {

    dispatch(removePokemonFavoritesThunk(pokemon.name))

    btn.style.filter = "brightness(1)";
    window.alert("Removido dos Favoritos !")
  };

  const brokenUrl = (url) => {
    
    let urlBroken = url.split("/");
    let id = urlBroken[urlBroken.length - 2];
    const pokImage = `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/${id}.png`;
    
    return pokImage;
  };

  return (
    <div className="pokemonsCardsList-content">
      {props.list.map((pokemons, index) => (
        <div className="pokemons-card" key={index}>
          <div className="likes-pokemon-content">
            <div onClick={(e) => setFavoritePokemon(pokemons, e.target)}>
              {pokemonFavorites.findIndex((favPokemon) => favPokemon.name === pokemons.name) < 0 ? likeIcon : alreadyLiked}
            </div>
            <div onClick={(e) => removefavoritePokemons(pokemons, e.target)}>
              {deslikeIcon}
            </div>
          </div>
          <img src={brokenUrl(pokemons.url)} alt="pokemon" />
          <p>{pokemons.name}</p>
        </div>
      ))}
    </div>
  );
};

export default PokemonsList;
