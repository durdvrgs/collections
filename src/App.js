import "./App.css";

import Links from "./routes/Links";
import Pages from "./routes/Switchs";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <Links />
      </header>
      <section className="App-section">
        <Pages />
      </section>
    </div>
  );
}

export default App;
