import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
import { BrowserRouter as Route } from "react-router-dom";
import { store } from "./redux";
import { Provider } from "react-redux";

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <Route>
        <App />
      </Route>
    </Provider>
  </React.StrictMode>,
  document.getElementById("root")
);

reportWebVitals();
